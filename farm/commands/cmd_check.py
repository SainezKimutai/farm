import click, time

from farm.services import svc_check


class Context:
    def __init__(self, directory):
        self.directory = directory
        self.check = svc_check.Check()


@click.group()
@click.option("-d", "--directory", type=str, help=" Example  'farm check -d harvest_two/ all' ")
@click.pass_context
def cli(ctx, directory):
    """Check submitted data"""
    ctx.obj = Context(directory)


@cli.command()
@click.pass_context
def all(ctx):
    """ Run all check commands """
    ctx.obj.check.all(directory = ctx.obj.directory)


@cli.command()
@click.pass_context
def crops(ctx):
    """ Multiple measurements for the same crop in a single farm """
    ctx.obj.check.duplicate_submissions(directory = ctx.obj.directory)


@cli.command()
@click.pass_context
def dry_w(ctx):
    """ Dry weight measurement exceeds the corresponding wet weight measurement """
    ctx.obj.check.dry_weight_vs_wet_weight(directory = ctx.obj.directory)


@cli.command()
@click.pass_context
def standard_dev(ctx):
    """ Dry weight is outside the standard deviation of all other submissions for the same crop """
    ctx.obj.check.dry_weight_standard_deviation(directory = ctx.obj.directory)


@cli.command()
@click.pass_context
def location(ctx):
    """ GPS coordinates of one farm are within 200 meters of another recorded farm """
    ctx.obj.check.gps_location(directory = ctx.obj.directory)


@cli.command()
@click.pass_context
def photos(ctx):
    """ Photo submitted is a duplicate of another photo that was submitted """
    ctx.obj.check.duplicate_photos(directory = ctx.obj.directory)
