import click, os, json, math, cv2, geopy.distance

import numpy as np

from farm.config import BASE_PATH, DEFAULT_DATA_PATH


class Check:

    def __init__(self,):
        self.default_directory = DEFAULT_DATA_PATH
        self.json_data = []
        self.deviated_dry_weights = []
        self.close_farms = []
        self.image_data = []
        self.duplicated_images = []


    def all(self, directory=None):
        self.check_zero(directory)
        self.check_one()
        self.check_two()
        self.check_three()
        self.check_four()
        self.check_five(directory)


    def duplicate_submissions(self, directory=None):
        self.check_zero(directory)
        self.check_one()


    def dry_weight_vs_wet_weight(self, directory=None):
        self.check_zero(directory)
        self.check_two()


    def dry_weight_standard_deviation(self, directory=None):
        self.check_zero(directory)
        self.check_three()


    def gps_location(self, directory=None):
        self.check_zero(directory)
        self.check_four()


    def duplicate_photos(self, directory=None):
        self.check_zero(directory)
        self.check_five(directory)


# Load the json files;
    def check_zero(self, directory):
        # get the file path
        file_path =  self.default_directory if directory == None else directory
        # load the json file
        file_name = "farm_data.json"
        self.json_data = self.load_farm_data(BASE_PATH + file_path + file_name)['harvest_measurements']


    def load_farm_data(self, file_directory):
        with open(file_directory) as file:
            data = json.load(file)
        return data


# Check 1 : Flag all submissions where there are multiple measurements for the same crop in a single farm
    def check_one(self):
        check_title = 'Multiple measurements for the same crop in a single farm'
        redundant_data =  list(filter(self.check_redundancy, self.json_data))
        if len(redundant_data) > 0 :
            self.flag_checks(redundant_data, check_title)
        else:
            self.no_issues(check_title)


    def check_redundancy(self, data_param):
        occurence = 0
        for item in self.json_data:
            if item['farm_id'] == data_param['farm_id'] and item['crop'] == data_param['crop'] :
                occurence += 1
        return data_param if occurence > 1 else False


# check 2: Flag all submissions where the dry weight measurement exceeds the corresponding wet weight measurement
    def check_two(self):
        check_title = 'Dry weight measurement exceeds wet weight'
        wrong_weights_data =  list(filter(self.check_weights, self.json_data))
        if len(wrong_weights_data) > 0 :
            self.flag_checks(wrong_weights_data, check_title)
        else:
            self.no_issues(check_title)


    def check_weights(self, item ):
        return item if item['dry_weight'] > item['wet_weight'] else False


# Check 3: Flag all submissions where the dry weight is outside the standard deviation of all other submissions for the same crop
    def check_three(self):
        check_title = 'Deviated dry weight'
        for item in self.json_data:
            similar_crops = self.get_similar_crop(item['crop'])
            dry_weight_list = list(map(lambda item: item["dry_weight"], similar_crops ))
            mean = self.calculate_mean(dry_weight_list)
            variance = self.calculate_variance(mean, dry_weight_list)
            standard_deviation = math.sqrt(variance)
            if item['dry_weight'] > (mean + standard_deviation) or (mean - standard_deviation) > item['dry_weight']:
                self.deviated_dry_weights.append(item)
        if len(self.deviated_dry_weights) > 0 :
            self.flag_checks(self.deviated_dry_weights, check_title)
        else:
            self.no_issues(check_title)


    def get_similar_crop(self, crop_name):
        similar_crop_list = []
        for item in self.json_data:
            if item['crop'] == crop_name:
                similar_crop_list.append(item)
        return similar_crop_list


    def calculate_mean(self, list_param):
        return sum(list_param) / len(list_param)


    def calculate_variance(self, mean, list_param):
        squared_list = list(map(lambda item: (item - mean)**2 , list_param))
        return sum(squared_list) / len(squared_list)


# Check 4: Flag all submissions where the GPS coordinates of one farm are within 200 meters of another recorded farm
    def check_four(self):
        check_title = 'Close Farms'
        for index in range(len(self.json_data) -1 ):
            self.check_for_close_farm(index)

        if len(self.close_farms) > 0 :
            self.flag_checks_close_farms(self.close_farms, check_title)
        else:
            self.no_issues(check_title)


    def check_for_close_farm(self, index):

        for next_index in range(index + 1, len(self.json_data)):
            current_submission = (self.json_data[index]['location'])
            next_submission =  (self.json_data[next_index]['location'])
            distance = geopy.distance.geodesic(current_submission, next_submission).m

            if distance < 201 and distance > -201:
                self.close_farms.append({'farm_one': self.json_data[index],  'farm_two': self.json_data[next_index], })


# Check 5: Flag all submissions where the photo submitted is a duplicate of another photo that was submitted.
    def check_five(self, directory):
        check_title = 'Duplicate Images'

        # get the file path
        images_folder =  self.default_directory if directory == None else directory

        print('loading images ...')
        for imagename in os.listdir(BASE_PATH + images_folder):
            if imagename.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')):
                img_url = BASE_PATH + images_folder + imagename
                self.image_data.append({"name": imagename,  "image": cv2.imread(img_url)})

        for index in range(len(self.image_data) -1 ):
            self.compare_images(index)

        if len(self.duplicated_images) > 0 :
            self.flag_checks_ducplicate_image(self.duplicated_images, check_title)
        else:
            self.no_issues(check_title)


    def compare_images(self, index):
        for next_index in range(index + 1, len(self.image_data)):
            original = self.image_data[index]["image"]
            duplicate =  self.image_data[next_index]["image"]

            # check if they have the same size and channels
            if original.shape == duplicate.shape:

                # Subtract the pixels if the images
                difference = cv2.subtract(original, duplicate)

                # A colored image has 3 channels (blue, green and red),
                # so the cv2.subtract() operation makes the subtraction for each single channel and we need to check if all the three channels are black (0 pexels value).
                b, g, r = cv2.split(difference)

                # if the pexels value is 0, (black image), it means the images are the same
                if cv2.countNonZero(b) == 0 and cv2.countNonZero(g) == 0 and cv2.countNonZero(r) == 0:
                    self.duplicated_images.append({"original": self.image_data[index]["name"], "duplicate": self.image_data[next_index]["name"] })


# Print results functions
    def flag_checks(self, list_param, check_title):
        print(" ")
        click.secho(f' {check_title} ', fg="cyan", bold=True, underline=True)
        print(" ")
        for item in list_param:
            click.echo(f'  \U0001f332  Crop: {item["crop"]}')
            click.echo(f'  \U0001f194  Farm Id: {item["farm_id"].upper()} ')
            click.echo(f'  \U0001f30a  Wet Weight: {item["wet_weight"]}')
            click.echo(f'  \u2600   Dry Weight: {item["dry_weight"]}')
            click.echo(f'  \U0001f6a9  Location: {item["location"]}')
            print(" ")
        print(" ")
        print(" ")
        print(" ")


    def flag_checks_close_farms(self, list_param, check_title):
        print(" ")
        click.secho(f' {check_title} ', fg="cyan", bold=True, underline=True)
        print(" ")
        for item in list_param:
            click.echo(f'  \U0001f6a9   Farm {item["farm_one"]["farm_id"].upper()} ({item["farm_one"]["crop"]}) is close to farm {item["farm_two"]["farm_id"].upper()} ({item["farm_two"]["crop"]})')
            print(" ")
        print(" ")
        print(" ")
        print(" ")


    def flag_checks_ducplicate_image(self, list_param, check_title):
        print(" ")
        click.secho(f' {check_title} ', fg="cyan", bold=True, underline=True)
        print(" ")
        for item in list_param:
            click.echo(f'  \U0001f4f7   Image {item["duplicate"]} is a duplicate of {item["original"]}')
            print(" ")
        print(" ")
        print(" ")
        print(" ")


    def no_issues(self, check_title):
        print(" ")
        click.secho(f' {check_title} ', fg="cyan", bold=True, underline=True)
        print(" ")
        click.echo(f'  	\u2713 Passed. No flags '.center(45, "*"))
