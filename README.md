# Farm Python CLI

## Python version
Python 3.8.5

## Project Setup

Clone the project
```
git clone https://gitlab.com/SainezKimutai/farm.git
```

Navigate to the project
```
cd farm
```

Create Virtual env
```
virtualenv venv
```

Activate Virtual env
```
source venv/bin/activate
```

Install requires modules
```
pip install --editable .
```


## Command Checks
All the five check commands are implemented.

### farm check
Lists all commands and options
```
farm check
```

### farm check all
Runs all command checks
```
farm check all
```

### Check 1: farm check crops
Flag all submissions where there are multiple measurements for the same crop in a single farm
```
farm check crops
```

### Check 2: farm check dry-w
Flag all submissions where the dry weight measurement exceeds the corresponding wet weight measurement
```
farm check dry-w
```

### Check 3: farm check standard-dev
Flag all submissions where the dry weight is outside the standard deviation of all other submissions for the same crop
```
farm check standard-dev
```

### Check 4: farm check location
Flag all submissions where the GPS coordinates of one farm are within 200 meters of another recorded farm
```
farm check location
```

### Check 5: farm check photos
Flag all submissions where the photo submitted is a duplicate of another photo that was submitted.
```
farm check photos
```

### Specifying path to a different data directory
Assumption: the base directory of the data is /farm/static/.
Place the folder containing the data in this directory, '/farm/static/' within the project
Use '-d' flag followed by the folder_name, and the last folder should end with '/'
e.g. '-d harvest_two/ '
```
farm check -d harvest_two/ all
```
